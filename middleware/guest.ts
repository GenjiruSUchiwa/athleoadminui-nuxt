import { useAuthStore } from "~/store/auth";

export default defineNuxtRouteMiddleware((to) => {
    const auth = useAuthStore()
    const router = useRouter()

    if(auth.isAuthenticated) {
        return router.push({
            path: "/",
        })
    }
})