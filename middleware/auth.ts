import { useAuthStore } from "~/store/auth";

export default defineNuxtRouteMiddleware((to, from) => {
    const auth = useAuthStore()
    const router = useRouter()
    console.log(auth.isAuthenticated)
    if(!auth.isAuthenticated) {
        return router.push({
            path: "/login",
            query: {
                next: to.path
            }
        })
    }
})