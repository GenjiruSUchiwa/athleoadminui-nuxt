import {computed, ref, Ref, watchEffect} from "vue";
import {type PaginationState} from "@tanstack/vue-table";
import {mapUsers} from "~/composables/mapUsers";
import {IdentityUser} from "~/types";

const DEFAULT_PAGE_COUNT = -1;
const DEFAULT_RESULT_COUNT = -1;

const baseUrl = "http://62.72.22.195:8080/api/v1";

export default function useService(pagination: Ref<PaginationState>, endpoint:Ref<string>) {
    const userAuth = useCookie('auth.user')
    const requestOptions = {
        headers: userAuth.value
            ? {
                'authorization': `${userAuth.value.token

                }`,
                'Content-type': 'application/json',
                'id': `${userAuth.value.id}`
            }
            : {'Content-type': 'application/json'},
    }
    const data: Ref<IdentityUser[]|null> = ref(null);
    const totalResultCount = ref(DEFAULT_RESULT_COUNT);
    const error = ref(null);
    const isLoading = ref(false);
    const request = ref<Promise<any> | null>(null);

    const requestParams = computed(() => {
        const { pageSize, pageIndex } = pagination.value;
        const currentPage = pageIndex + 1;

        return {
            limit: pageSize.toString(),
            page: currentPage.toString(),
        };
    });
    console.log(endpoint)

    const url = computed(() => {
        const searchParams = new URLSearchParams(requestParams.value);
        return `${baseUrl}${endpoint.value}?${searchParams}`;
    });

    const pageCount = computed(() => {
        const { pageSize } = pagination.value;

        return Math.ceil(totalResultCount.value / pageSize);
    })

    watchEffect(() => {
        isLoading.value = true;
        request.value = fetch(url.value, requestOptions)
            .then(async (response) => {
                const responseData = await response.json();
                if (response.ok) {
                    data.value = mapUsers(responseData.data.docs);
                    error.value = null;
                    totalResultCount.value =
                        Number(responseData.data.total) ?? DEFAULT_PAGE_COUNT;
                } else {
                    throw new Error("Network response was not OK");
                }
            })
            .catch((error) => {
                error.value = error;
                data.value = null;
                totalResultCount.value = DEFAULT_PAGE_COUNT;

                console.log("error!", error);
            })
            .finally(() => {
                isLoading.value = false;
            });
    });

    return {
        data,
        totalResultCount,
        pageCount,
        error,
        isLoading,
    };
}




