// api.js

export function useApi() {
    const apiUrl = 'http://62.72.22.195:8080/api/v1';
        const userAuth = useCookie('auth.user')
    async function fetchData(endpoint, method = 'GET', data = null) {
        const accessToken = userAuth.value.token;
        const id = userAuth.value.id;

        const headers = {
                    'Authorization': `${accessToken}`,
            'Content-Type': 'application/json',
            'id': `${id}`,
            // Add other headers as needed
        };

        const requestOptions = {
            method,
            headers,
            body: data ? JSON.stringify(data) : null,
        };

        try {
            const response = await fetch(`${apiUrl}/${endpoint}`, requestOptions);
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }

            return response.json();
        } catch (error) {
            console.error('Fetch error:', error);
            throw error;
        }
    }

    return {
        fetchData,
    };
}
