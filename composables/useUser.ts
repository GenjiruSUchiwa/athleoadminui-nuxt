// composables/useUser.ts
import {Ref, ref, UnwrapRef} from 'vue'
import {mapUsers} from "~/composables/mapUsers";
import {RouteParamValue} from "vue-router";
import {IdentityUser} from "~/types";

export default function useUser(id: UnwrapRef<string | RouteParamValue[]> ) {

    const userAuth = useCookie('auth.user')
    const user : Ref<IdentityUser|null> = ref(null)
    const error : Ref<unknown|string|null> = ref(null)
    let isLoading :Ref<boolean> = ref(false)

    const fetchUser = async () => {
        const requestOptions = {
            method: 'GET',
            headers: userAuth.value
                ? {
                    'authorization': `${userAuth.value.token

                    }`,
                    'Content-type': 'application/json',
                    'id': `${userAuth.value.id}`
                }
                : {'Content-type': 'application/json'},
        }
        isLoading.value = true
        try {
            const response = await fetch(`http://62.72.22.195:8080/api/v1/admin/getuserinfos?user_id=${id}`,requestOptions)

            if (!response.ok) {
                throw new Error('An error occurred')
            }
            const responseData = await response.json()
            user.value = mapUsers(responseData.data)[0];
        } catch (err) {
            error.value = err.message
        }
        finally {
            isLoading.value = false;
        }

    }
    const deleteUser = async () => {
        const requestOptions = {
            method: 'POST',
            headers: userAuth.value
                ? {
                    'authorization': `${userAuth.value.token

                    }`,
                    'Content-type': 'application/json',
                    'id': `${userAuth.value.id}`
                }
                : {'Content-type': 'application/json'},
        }
        try {
            const response = await fetch(`http://62.72.22.195:8080/api/v1/admin/removeuser?user_id=${id}`,requestOptions)

            if (!response.ok) {
                throw new Error('An error occurred')
            }
            console.log("user deleted...")
        } catch (err) {
            error.value = err.message
        }
        finally {
        }
    }
    const confirmUser = async () => {
        const requestOptions = {
            method: 'POST',
            headers: userAuth.value
                ? {
                    'authorization': `${userAuth.value.token

                    }`,
                    'Content-type': 'application/json',
                    'id': `${userAuth.value.id}`
                }
                : {'Content-type': 'application/json'},
        }
        try {
            const response = await fetch(`http://62.72.22.195:8080/api/v1/admin/validateuser?user_id=${id}`,requestOptions)

            if (!response.ok) {
                throw new Error('An error occurred')
            }
            console.log("user deleted...")
        } catch (err) {
            error.value = err.message
        }
        finally {
        }
    }


    const sendMessage = async (title:string, content:string, to:string) => {
        const requestOptions = {
            method: 'POST',
            headers: userAuth.value
                ? {
                    'authorization': `${userAuth.value.token

                    }`,
                    'Content-type': 'application/json',
                    'id': `${userAuth.value.id}`
                }
                : {'Content-type': 'application/json'},
        }
        try {
            const response = await fetch(`http://62.72.22.195:8080/api/v1/admin/adminsendmail?title=${title}&message=${content}&email_to=${to}`,requestOptions)

            if (!response.ok) {
                throw new Error('An error occurred')
            }
            console.log("user deleted...")
        } catch (err) {
            error.value = err.message
        }
        finally {
        }
    }

    return { user, error, isLoading, deleteUser, fetchUser, confirmUser, sendMessage }
}
