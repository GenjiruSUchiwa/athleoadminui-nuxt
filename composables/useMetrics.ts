// composables/useUser.ts
import {Ref, ref} from 'vue'

export default function useMetrics() {

    const userAuth = useCookie('auth.user')
    const data : any = ref(null)
    const error : Ref<unknown|string|null> = ref(null)
    let isLoading :Ref<boolean> = ref(false)

    const getMetrics = async () => {
        const requestOptions = {
            method: 'GET',
            headers: userAuth.value
                ? {
                    'authorization': `${userAuth.value.token

                    }`,
                    'Content-type': 'application/json',
                    'id': `${userAuth.value.id}`
                }
                : {'Content-type': 'application/json'},
        }
        isLoading.value = true
        try {
            const response = await fetch(`http://62.72.22.195:8080/api/v1/admin/getmetrics`,requestOptions)

            if (!response.ok) {
                throw new Error('An error occurred')
            }
            const responseData = await response.json()
            data.value = responseData.data
            console.log(data.value)
        } catch (err) {
            error.value = err.message
        }
        finally {
            isLoading.value = false;
        }

    }

    onMounted(async () => (await getMetrics()))

    return { data, error, isLoading }
}
