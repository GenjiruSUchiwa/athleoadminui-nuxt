import {IdentityUser, ResponseUser} from "~/types";

export function mapUsers(users: ResponseUser[]) : IdentityUser[] {
    return users.map(item =>
        {
            return {
        id: item._id,
        role: item.userRole ?? "N/A",
        profession: fromEmpty(item.profession) ?? "N/A",
        specialty: fromEmpty(item.specialty) ?? "N/A",
        idendity_paper: fromEmpty(item.idendityPaper) ?? "N/A",
        sport: item.sport ?? "N/A",
        idendity_file: item.idendityFile ? getUrl(item.idendityFile) : "N/A",
        account_approved: item.account_approved ?? false,
        is_email_verified: item.isEmailVerified ?? false,
        country: item.country ?? "N/A",
        city: item.city ?? "N/A",
        firstname: item.firstname ?? "N/A",
        surname: item.surname ?? "N/A",
        social_username: item.social_username,
        phone: item.phone ?? "N/A",
        fullname: getFullName(item.firstname, item.surname ?? ''),
        longitude: item.longitude ?? "N/A",
        latitude: item.latitude ?? "N/A",
        photo: item.photo ? getUrl(item.photo) : "N/A",
        email: item.email ?? "N/A",
        social_twitter: item.social_twitter ?? null,
        social_facebook: item.social_facebook ?? null,
        social_linkedin: item.social_linkedin ?? null,
        social_instagram: item.social_instagram ?? null,
        date_birth: item.date_birth ?? null,
        date_inscription: item.date_inscription ?? null,
    }})
}

function getFullName(firstname: string | undefined, lastname: string | undefined) {
    return `${firstname ?? 'Unknown'} ${lastname ?? 'Unknown'}`
}

function fromEmpty(str: string){
    if(str == null || str.trim() === '')
    {
        return "N/A"
    }
    return str
}

function getUrl(str:string)
{
    const empty = fromEmpty(str)
    if(empty === "N/A")
    {
        return empty
    }
    return `http://62.72.22.195:8080/medias/${str}`
}
