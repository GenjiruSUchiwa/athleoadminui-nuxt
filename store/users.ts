import {defineStore} from "pinia";
import {useApi} from "~/composables/useApi.ts";

export let useUsersStore = defineStore('users', {
    state: () => {
        return {
            users: [],
            total: 0,
            limit: 0,
        }
    },
    actions: {
        async fill(page =2 ) {
            const {fetchData} = useApi();
            const {data} = await fetchData(`admin/listallusers?page=${page}`);
            const newUsers = data.docs.map(item => ({
                id: item._id,
                role: item.userRole ?? null,
                profession: item.profession ?? null,
                specialty: item.speciality ?? null,
                idendity_paper: item.idendityPaper ?? null,
                sport: item.sport ?? null,
                idendity_file: item.idendityFile ?? null,
                account_approved: item.account_approved ?? false,
                is_email_verified: item.isEmailVerified ?? false,
                country: item.country ?? null,
                city: item.city ?? null,
                firstname: item.firstname ?? null,
                surname: item.surname ?? null,
                phone: item.phone ?? null,
                fullname: getFullName(item.firstname,item.surname??''),
                longitude: item.longitude ?? null,
                latitude: item.latitude ?? null,
                photo: item.photo ?? null,
                email: item.email ?? null,
                date_birth: item.date_birth ? new Date("2004-10-14T00:00:00.000Z") : null,
                date_inscription: item.date_inscription ? new Date("2023-10-07T07:28:24.509Z") : null,
            }))
            this.users = newUsers
            this.total = data.total
            this.limit = data.limit
        }
    },
    getters: {
        pageCount: (state) => state.count * 2,
    },
})

function getFullName(firstname:string, lastname: string)
{
    return `${firstname} ${lastname}`
}