import {FunctionalComponent, HTMLAttributes, VNodeProps} from "vue";

export interface NavigationItem {
    name: string
    href?: string
    icon: FunctionalComponent<HTMLAttributes & VNodeProps>
    current: boolean
    group: string
    children?: NavigationSubItem[]
}

export interface NavigationSubItem {
    name: string
    href: string
    count: string
    current: boolean
}

export interface UserNavigation {
    name: string,
    href: string
}


export type Navigation = NavigationItem[]


export interface IdentityUser {
    date_birth: Date | null;
    country: string;
    specialty: string;
    firstname: string;
    role: string;
    city: string;
    is_email_verified: boolean;
    latitude: string;
    idendity_paper: string;
    social_linkedin: string;
    social_instagram: string;
    surname: string;
    id: string;
    social_username: string;
    email: string;
    longitude: string;
    profession: string;
    photo: string;
    account_approved: boolean;
    social_twitter: string;
    phone: string;
    social_facebook: string;
    idendity_file: string;
    date_inscription: Date | null;
    fullname: string;
    sport: string
}


export interface ResponseUser {
    _id: string;
    userRole: string;
    profession: string;
    specialty: string;
    idendityPaper: string;
    sport: string;
    idendityFile: string;
    account_approved: boolean;
    isEmailVerified: boolean;
    country: string;
    city: string;
    firstname: string;
    surname: string;
    phone: string;
    longitude: string;
    latitude: string;
    photo: string;
    password: string;
    email: string;
    auth_token: string;
    social_id: string;
    social_username: string;
    date_birth: Date;
    date_inscription: Date;
    __v: number;
    social_twitter: string;
    social_facebook: string;
    social_linkedin: string;
    social_instagram: string;
}

